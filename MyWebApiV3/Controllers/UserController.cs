﻿using ConectarDatos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyWebApiV3.Controllers
{
    public class UserController : ApiController
    {
        private UserEntities ConexionDB = new UserEntities();

        // Visualiza todos los registros (API/USER)
        [HttpGet]
        public IEnumerable<User> Get()
        {
            using (UserEntities VerDatosUser = new UserEntities())
            {
                return VerDatosUser.Users.ToList();
            }

        }

        //visualiza solo un registro (API/USER/1)
        [HttpGet]
        public User Get(int id)
        {

            using (UserEntities Vertodouser = new UserEntities())
            {
                return Vertodouser.Users.FirstOrDefault(e => e.ID_USER == id);
            }

        }

        //POST GRABAR NUEVOS REGISTROS EN BD
        [HttpPost]
        public IHttpActionResult AddUser([FromBody]User usu)
        {
            if (ModelState.IsValid)
            {
                ConexionDB.Users.Add(usu);
                ConexionDB.SaveChanges();
                return Ok(usu);
            }
            else
            {
                return BadRequest();
            }
        }

        //METODO PARA EDITAR O MODIFICAR
        [HttpPut]
        public IHttpActionResult ActualizarUsers(int id, [FromBody]User usu)
        {
            if (ModelState.IsValid)
            {
                var UsersExiste = ConexionDB.Users.Count(c => c.ID_USER == id) > 0;

                if (UsersExiste)
                {
                    ConexionDB.Entry(usu).State = EntityState.Modified;
                    ConexionDB.SaveChanges();

                    return Ok();
                }
                else
                {
                    return NotFound();
                }

            }
            else
            {
                return BadRequest();
            }
        }

        //BORRA UN REGISTRO
        [HttpDelete]
        public IHttpActionResult EliminarUser (int id)
        {
            var usu = ConexionDB.Users.Find(id);
            if (usu != null)
            {
                ConexionDB.Users.Remove(usu);
                ConexionDB.SaveChanges();

                return Ok(usu);
            }
            else
            {
                return NotFound();
            }
        }

    }
}
